# Service - Oriented Architecture (SOA)
---
## INTRODUCTION
Old client-server models used to be highly coupled and involved host-centric and time-shared computing, which were characterized by simple, straight-forward software. In comparison new models having *service oriented architecture* involve the modular pieces of software contained in and distributed over seperate servers. And these pieces of software interconnected incorporate together using new and better message based communication.

*SOA* could be defined as an approach for designing, developing, deploying and maintaining software systems that are based on distributed resources and assets.

----
## Basic Concept
Assume we have a lot of resources that we could take advantage of and it would help us in our domain, let's call these resources **services** . Applications in SOA are built based on these services.
SOA allows for the re-use of existing assets where new services could be created from an existing IT infrastructure of systems.

> SOA is an architectural pattern for building software systems based on loosely coupled service components.

### **Why SOA?**
The motives behind adoption of *service orientated architecture* revolves around three major drivers. 

* First, distributed systems. This is one of the main reason for SOA adoption. 

* Second, different owners or different ownership domains. 
As businesses and software systems grow and more teams or even companies become involved in the development and maintenance of software systems, *SOA* represents an ideal solution for such cases. 

* And last but not least, heterogeneity, global and large complex systems tend to like harmony. Different platforms and technologies are used to solve different problems in the system. This introduces many challenges, however SOA brings harmony back through smooth integration of different parts into one single system. 

*So these are the major motives for adopting SOA.*

---

## Components
![SOA SYSTEM](./soa.png)

* ### Services
        Developers have started to divide monolithic application solutions into modular pieces and made them accessible at specific, well-defined
        endpoints. This made the software more flexible when obstructed. These pieces of software are called services. 
        Services are mainly used in SOA because they can be easily reused.

* ### Enterprise Service Bus (ESB)
        The ESB is the backbone of every SOA system. It's responsibility is handling the integration part of the system.
        ESB is part of the reason why SOA is chosen or preferred over other architectural patterns.
    **Responsibilities**

    * Providing Connectivity
    * Data transformation & Message Conversion
    * Routing
    * Security
    * Service Management
    * Monitoring & Logging

* ### Service Description Respository/Registry (SDR)
        This is how service consumers discover services and learn the proper way to connect and communicate with them. This component becomes
        necessary when a SOA system grows large and complex.

* ### Service Consumers
        They could be internal services in the system as well as external or third-party clients. 

---

## SOA Principles

* **Standard Service Contracts**

* **Interoperability**

* **Composability**

* **Abstraction**

* **Discoverability**

* **Statelessness**
---

## Pros & Cons

### **Advantages**
* **Reusability**
* **Maintainability**
* **Scalability & Availability**
* **Platform Independence**
* **Responsiveness and Improved Time-to-Market**


### **Limitations**
* **Complex Service Management**
* **Increased Overhead**
* **Vendor Lock-in**
* **Upfront Investment**

---

## References

* [Service-Oriented Architecture (SOA) and Web Services](https://www.oracle.com/technical-resources/articles/javase/soa.html "SOA-Oracle")
* [The Open Group - SOA](https://publications.opengroup.org/downloadable/download/link/id/MC4zNzIxNjgwMCAxNjEyOTUyMjkyNzY0NjEzNzg0Mzk3MjQ0/ "Service-Oriented Architecture")
* [ The TechCave ]( https://www.youtube.com/watch?v=jNiEMmoTDoE "SOA")

---
---
